#pragma once
//calculator1

#include <iostream>

struct Expr;
struct Int;
struct Add;
struct Sub;
struct Mul;
struct Div;

//----------------------------------------------------
struct Expr{
	//object parameters
	
	//destructor
	virtual ~Expr() = default;
	
	//constructor clone
	virtual Expr* clone() const = 0;
	
	//print function
	virtual void print(std::ostream& os) const = 0;
	
	//evaluate function
	virtual int evaluate() const = 0;
};

//-----------------------------------------------------
struct Int : Expr{
	//object parameters
	Int(int n) : val (n) {}
	
	//destructor
	
	//constructor clone
	Int * clone() const override {
		return new Int(*this);
	}
	
	//print function
	void print(std::ostream & os) const override {
		os << val;
	}
	
	//evaluate function
	int evaluate() const override {
		return val;
	}
};
//-----------------------------------------------------
struct Add: Expr{
	//object parameters
	
	//destructor
	
	//constructor clone
	Add* clone() const override;
	
	//print function
	void print(std::ostream& os) const override;
	
	//evaluate function
	
};

//-----------------------------------------------------
struct Sub: Expr{
	//object parameters
	
	//destructor
	
	//constructor clone
	Sub* clone() const override;
	
	//print function
	void print(std::ostream& os) const override;
	
	//evaluate function
};

//-----------------------------------------------------
struct Mul: Expr{
	//object parameters
	
	//destructor
	
	//constructor clone
	Mul* clone() const override;
	
	//print function
	void print(std::ostream& os) const override;
	
	//evaluate function
};

//-----------------------------------------------------
struct Div: Expr{
	//object parameters
	
	//destructor
	
	//constructor clone
	Div* clone() const override;
	
	//print function
	void print(std::ostream& os) const override;
	
	//evaluate function
};
