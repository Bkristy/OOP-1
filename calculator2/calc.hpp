#pragma once
//calculator2
struct expr;
struct int_expr;
struct add_expr;
struct sub_expr;
struct mul_expr;
struct div_expr;
struct rem_expr;
struct neg_expr;

struct visitor {
  virtual void visit(int_expr*) = 0;
  virtual void visit(add_expr*) = 0;
  virtual void visit(sub_expr*) = 0;
  virtual void visit(mul_expr*) = 0;
  virtual void visit(div_expr*) = 0;
  virtual void visit(rem_expr*) = 0;
  virtual void visit(neg_expr*) = 0;
};

struct expr {
  virtual ~expr() { }
  virtual void accept(visitor&) = 0;
};

struct unary_expr : expr {
  unary_expr(expr* e1) : e1(e1) { }
  expr* e1;
};

struct binary_expr : expr {
  binary_expr(expr* e1, expr* e2) : e1(e1), e2(e2) { }
  expr* e1;
  expr* e2;
};

struct int_expr : expr {
  int_expr(int n) : val(n) { }
  void accept(visitor& v) override { v.visit(this); }
  int val;
};

struct add_expr : binary_expr {
  using binary_expr::binary_expr;
  void accept(visitor& v) override { v.visit(this); }
};

struct sub_expr : binary_expr {
  using binary_expr::binary_expr;
  void accept(visitor& v) override { v.visit(this); }
};

struct mul_expr : binary_expr {
  using binary_expr::binary_expr;
  void accept(visitor& v) override { v.visit(this); }
};

struct div_expr : binary_expr {
  using binary_expr::binary_expr;
  void accept(visitor& v) override { v.visit(this); }
};

struct rem_expr : binary_expr {
  using binary_expr::binary_expr;
  void accept(visitor& v) override { v.visit(this); }
};

struct neg_expr : unary_expr {
  using unary_expr::unary_expr;
  void accept(visitor& v) override { v.visit(this); }
};
