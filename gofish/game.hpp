//game.hpp

#pragma once

#include "player.hpp"
#include "deck.hpp"

#include <unistd.h> //debug

struct Game{
    Playerdeck p1; //current hand for p1
    Playerdeck transfer1; //the intermediate vector
    Playerdeck played1; //pool of cashed in cards, ie set of 4
    Playercardcount cardcount1; //int //will contain a list of all cards 0 - 4


    Playerdeck p2;
    Playerdeck transfer2;
    Playerdeck played2;
    Playercardcount cardcount2;






    Deck deck = make_standard_deck(); //starting deck "go fish pool"
    //Deck pool; //


};

void divy_cards(Deck& deck, Deck& decka, Deck& deckb){
    //each player gets 5 cards at beginning of game
    //for the game of go fish

    shuffle_deck(deck);

    for(int i = 1; i <= 5; ++i){
        decka.push_back(deck.back());
        deck.pop_back();
        deckb.push_back(deck.back());
        deck.pop_back();
    }



}//end divy_cards



void run_game(){
    //game object
    Game game;

    //starting hand
    divy_cards(game.deck, game.p1, game.p2);
    cout << endl << "P1 HAND: ";
    print_deck(game.p1);
    cout << endl << "P2 HAND: ";
    print_deck(game.p2);


    while(game.p1.size() != 0 || game.p2.size() != 0){
//        cout << "hello loop" << endl;
//        usleep(10000);

        //while both players still have cards

            //need a method of determining the number of matching card suits
            //in a vector

            //need a way to remove items in the middle of the vector



            //player checks to see if they have 4 of a kind
            //to cash them in

            //vector check to see what their closest hand to 4 is
            // if they have 2 that are 3 cards then they choose the highest
            // wont choose same card after go fish consecutivly,

            //ask for that card?

            //True
                //take the card from other player

            //false
                //if cards are in deck still draw one

                //else do nothing







    }
    if (game.played1.size() > game.played2.size())
        cout << endl << "player 1 wins" << endl;
    else if (game.played1.size() < game.played2.size())
        cout << endl << "player 2 wins" << endl;
    else if (game.played1.size() == game.played2.size())
        cout << endl << "players Tie" << endl;
    else
        cout << endl << "Errrrrr" << endl;
}//end of run_game()



void get_card_count(Playerdeck& pdeck, Playercardcount& pcount){

    //erases old values of pcount
    for (int j = 0; j <= pdeck.size(); ++j){
        pcount[j] = 0;

    //iterates through player deck and updates pcount
    }
    for (int k = 0; k < pdeck.size(); ++k){
            switch (pdeck[k].get_rank()){
            case Ace:
                //ace += 1;
                pcount[1] += 1;
            case Two:
                //two += 1;
                pcount[2] += 2;
            case Three:
                //three += 1;
                pcount[3] += 3;
            case Four:
                //four += 1;
                pcount[4] += 3;
            case Five:
                //five ++;
                pcount[5] += 3;
            case Six:
                //six ++;
                pcount[6] += 3;
            case Seven:
                //seven ++;
                pcount[7] += 3;
            case Eight:
                //eight += 1;
                pcount[8] += 3;
            case Nine:
                //nine += 1;
                pcount[9] += 3;
            case Ten:
                //ten += 1;
                pcount[10] += 3;
            case Jack:
                //jack += 1;
                pcount[11] += 3;
            case Queen:
                //queen += 1;
                pcount[12] += 3;
            case King:
                //king += 1;
                pcount[13] += 3;

            }//end of switch
        }//end of for loop
}//end of get_card_count


void extract_cards(Playerdeck& pdeck, Playercardcount& pcount, Playerdeck& ptransfer, Playerdeck& played,Rank rnk){
    if (pcount[rnk] == 4){
        for (int k = pdeck.size(); k > 0; --k){
            if (pdeck[k].get_suit() == rnk){
                played.push_back(pdeck.back());
                pdeck.pop_back();
            }
            else{
                ptransfer.push_back(pdeck.back);
                pdeck.pop_back();
            }

        }//end of for loop
            //<>//need a method of transferring ptransfer back to pdeck

    }//end of conditional statement
    else
        cout << "you don't have enough " << rank_list[rnk] << " to discard" << endl;
}//end of extract_cards

