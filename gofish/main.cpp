//main.cpp

#include "game.hpp"

#include <iostream>
#include <random>
#include <ctime>

using namespace std;

minstd_rand prng;



int main()
{
    srand(time(nullptr));
    prng.seed(rand());

    run_game();


    return 0;
}



