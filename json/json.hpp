#pragma once
#include <iostream>
#include <iomanip>

struct Null;
struct Bool;
struct Number;
struct Array;
struct Object;
struct String;
struct Value;


struct Value{
	//destructor
	virtual ~Value() {}
	
	//constructor
	virtual Value* clone() const = 0;
	
	//print
	virtual void print() const = 0;
	
};

struct Null : Value {
	Null(){}
	
	void print() const{
		std::cout << "null";
	}
	
};
	
struct String : Value {
	
};
	
struct Bool : Value {
	Bool(bool b) : val(b){}
	
	void print() const 
	if val == true
		std::cout << "true";
	else
		std::cout << "false";
	
};


struct Number : Value {
	Number(double d) : val(d) {}
	
	void print() const override {
		std::cout << val;
	}
	
};
struct Array : Value {
	Array() = default;
	
};
struct Object : Value {
	Object() = default;
	~Object() {}
	
};
