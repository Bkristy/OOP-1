//card.hpp

#pragma once

#include <iostream>
#include <string>
#include <iosfwd>
#include <utility>


using namespace std;

enum Rank
{
  Ace = 1,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King
};

enum Suit
{
  Hearts = 1,
  Diamonds,
  Clubs,
  Spades
};


string rank_list[] = {"Null", "A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"};
string suit_list[] = {"Null", "H", "D", "C", "S"};

class Card{
public:
    Card() = default;

    Card(Rank r, Suit s): rank(r), suit(s) //AS
        { }

    Rank get_rank() const {
        return rank;
        }

    Suit get_suit() const {
        return suit;
        }

    Rank set_rank(Rank r){
        rank = r;
        return rank;
    }

    Suit set_suit(Suit s){

        suit = s;
        return suit;
    }

    void print_card(){
    //cout << rank_list[(rank)] << " of " << suit_list[(suit)] << " ";
    cout << "[" << rank_list[(rank)]  << suit_list[(suit)] << "] ";
    }

private:
    Rank rank;
    Suit suit;
};

bool operator==(Card a, Card b){
    if (a.get_rank() == b.get_rank())
        return true;
    else
        return false;
}

bool operator!=(Card a, Card b){
    if (a.get_rank() != b.get_rank())
        return true;
    else
        return false;
}

bool operator<(Card a, Card b){
    if (a.get_rank() < b.get_rank())
        return true;
    else
        return false;
}

bool operator>(Card a, Card b){
    if (a.get_rank() > b.get_rank())
        return true;
    else
        return false;
}


//<>//needed?
//ostream& operator<<(std::ostream& os, Card c);
//ostream& operator<<(std::ostream& os, Rank r);
//ostream& operator<<(std::ostream& os, Suit r);




