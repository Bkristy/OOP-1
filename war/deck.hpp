//deck.hpp

#pragma once

#include "card.hpp"

#include <vector>
#include <random>
#include <algorithm>

using namespace std;


using Deck = vector<Card>;


Deck make_standard_deck(){

    Deck deck;

    for (int j = Hearts; j <= Spades; ++j){
        for (int i = Ace; i <= King; ++i){
            //scout << i << ' ' << j << endl; //debug
            Card c(static_cast<Rank>(i), static_cast<Suit>(j)); //AS
            deck.push_back(c);
        }
    }

    return deck;

};


Deck make_game_deck();


void shuffle_deck(Deck& deck){

    extern minstd_rand prng;

    shuffle(deck.begin(), deck.end(), prng);

};

void print_deck(Deck& deck){

    for (int k = 0; k < deck.size(); ++k){
            deck[k].print_card();
        }

};

