//game.hpp

#pragma once

#include "player.hpp"
#include "deck.hpp"

#include <unistd.h> //debug

struct Game{
    Playerdeck p1;
    Playerdeck p2;

    Deck deck = make_standard_deck();
    Deck pool; //war spoils pool


};

void divy_cards(Deck& deck, Deck& decka, Deck& deckb){

    shuffle_deck(deck);
    while(deck.size() != 0){
    Card a = deck.back();
    deck.pop_back();
    decka.push_back(a);


    Card b = deck.back();
    deck.pop_back();

    deckb.push_back(b);
    }
}

void run_game(){
    //setup stuff
    Game game;
    //cout << endl << "std deck creation" << endl;
    //print_deck(game.deck);
    //cout << endl << "----------" << endl; //<debug>//

    divy_cards(game.deck, game.p1, game.p2);

    //cout << endl << "Player 1's card pool" << endl;
    //print_deck(game.p1); //<debug>//
    //cout << endl << "----------" << endl; //<debug>//
    //cout << endl << "Player 2's card pool" << endl;
    //print_deck(game.p2); //<debug>//
    //cout << endl;

    Card p1_hand;
    Card p2_hand;
    int number_of_turns = 0;
    int number_of_wars = 0;

    //bad
    extern int turn_count;
    extern int war_count;
    extern int tie_count;
    extern int p1_count;
    extern int p2_count;

    //main game logic
    while(game.p1.size() > 0 && game.p2.size() > 0){

        //usleep(100); //<debug>//

        p1_hand = game.p1.back();
        game.p1.pop_back();
        p2_hand = game.p2.back();
        game.p2.pop_back();

//        cout << game.p1.size() + 1;//<debug>//
//        cout << " P1 ";//<debug>//
//        p1_hand.print_card();//<debug>//
//        cout << "| ";//<debug>//
//        p2_hand.print_card();//<debug>//
//        cout << "P2 ";//<debug>//
//        cout << game.p2.size() + 1 << endl;//<debug>//

        //expedite common outcomes
        if (p1_hand > p2_hand){
            game.p1.push_back(p1_hand);
            game.p1.push_back(p2_hand);
            number_of_turns += 1;
        }

        if (p1_hand < p2_hand){
            game.p2.push_back(p1_hand);
            game.p2.push_back(p2_hand);
            number_of_turns += 1;
        }

        //war while loop
        while(p1_hand == p2_hand){
            number_of_wars += 1;
            number_of_turns += 1;
            //cout << endl << "WAR!" << endl;
            game.pool.push_back(p1_hand);
            game.pool.push_back(p2_hand);

            if (game.p1.size() == 0 || game.p2.size() == 0)
                break;

            p1_hand = game.p1.back();
            game.p1.pop_back();
            p2_hand = game.p2.back();
            game.p2.pop_back();

            if (p1_hand > p2_hand){
                //give player 1 the hand
                game.p1.push_back(p1_hand);
                game.p1.push_back(p2_hand);

                //give player 2 the war pool
                while(game.pool.size() != 0){
                    game.p1.push_back(game.pool.back());
                    game.pool.pop_back();
                }
                }

            if (p1_hand < p2_hand){
                //give player 2 the hand
                game.p2.push_back(p1_hand);
                game.p2.push_back(p2_hand);

                //give player 2 the war pool
                while(game.pool.size() != 0){
                    game.p2.push_back(game.pool.back());
                    game.pool.pop_back();
                }
                }

            }//end of while war

        shuffle_deck(game.p1);
        shuffle_deck(game.p2);
        }//end of main game logic

    if (game.p1.size() == 0 && game.p2.size() == 0)     //Condition 1
        //cout << endl << "TIE" << endl;
        tie_count += 1;
    else if (game.p1.size() == 0 && game.p2.size() > 0) //Condition 2
        //cout << endl << "P2 WIN" << endl;
        p2_count += 1;
    else if (game.p1.size() > 0 && game.p2.size() == 0) //Condition 3
        //cout << endl << "P1 WIN" << endl;
        p1_count += 1;
    else                                                //Condition 4th wall
        cout << endl << "ERRRRR" << endl;

    //cout << "turns: " << number_of_turns << endl; //<debug>//
    //cout << " wars: " << number_of_wars << endl; //<debug>//
    turn_count += number_of_turns;
    war_count += number_of_wars;

    }//end of run_game()





