//main.cpp

//#include "card.hpp"
//#include "deck.hpp"
#include "game.hpp"

#include <iostream>
#include <random>
#include <ctime>

using namespace std;

minstd_rand prng;
//bad
int turn_count = 0;
int war_count = 0;
int tie_count = 0;
int p1_count = 0;
int p2_count = 0;

int main()
{
    srand(time(nullptr));
    prng.seed(rand());

    int iteration = 0;
    int game_count = 10000;

    while(iteration < game_count){
        run_game();
        iteration += 1;
    }

    int avg_turns = turn_count / game_count;
    int avg_wars = war_count / game_count;

    cout << "Average number of turns: " << avg_turns << endl;
    cout << " Average number of wars: " << avg_wars << endl;
    cout << "Player 1 wins: " << p1_count << endl;
    cout << "Player 2 wins: " << p2_count << endl;
    cout << "Ties: " << endl;


    return 0;
}



